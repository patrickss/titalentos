# Exempolos de Markdown

Markdown é uma linguagem de marcação muito usada para documentação técnica.

Paragrafos de texto são simples de criar. Basta separá-los por uma linha em branco.

## Este é um cabeçalho de nível 2
texto, texto, texto

### este é um cabeçaçho de nível 3 
texto, texto

#### nível 4
##### Nível 5
###### Nível 6

## Listas

Listas são simples: basta usar 1. ou - antes do texto

- lista
- lista

### lista ordenada
1. lista
1. lista

## Destaque de texto

Destaque de textos servem para chamar atenção:

> Tudo que estiver nesta linha (até um ENTER) vai ficar em destaque

## Caixas de código

Podemos incluir código escrito em  diversas linguagens de programação com destaque de sintaxe, usa-se ```, e o código fica dentro das crases.

``` js
for(i=0 i>10; i++) {
console.log("Oi, oi, oi")
}
```
- Bash
``` bash
cp arquivo.txt -/testes
```

## Links
Link para o repositório do projeto

[Repositório do curso de Introdução à Programação](https://codeberg.org/grz/iprog)

## Imagens

Imagens para ilustrar a documentação

![Logotipo do bash](bash-logo-web.png)


## Salva e faz commit 
.