const rl = require('readline-sync')
const comidas = ['Lasanha','Pizza', 'Macarrão', 'Sushi', 'Lentilha']

console.log(comidas)

console.log(
    ` Estas são minhas comidas preferidas:`
)  
console.log(
    `
    ${comidas[0]}
    ${comidas[1]}
    ${comidas[2]}
    ${comidas[3]}
    ${comidas[4]}
    `
)

const preferida = rl.question("Qual a sua comida preferida? ")
comidas[1] = preferida

console.log(comidas)

for (let i = 0; i < comidas.length; i++) {
    const element = comidas[i];
    console.log(element)
}