const rl = require('readline-sync')

let frutas = ["Banana", "Morango", "Abacaxi", "Laranja", "Ameixa"]

const indiceAbacaxi = frutas.indexOf('Abacaxi')
console.log(`Abacaxi foi encontrado no indice ${indiceAbacaxi}, o tamanho do array é ${frutas.length}`)

console.log()
console.log()

for (i = 0; i < frutas.length; i++) {
    let fruta = frutas[i]
    if (fruta=='Abacaxi') {
        break
    }
}
console.log(`Abacaxi foi encontrado no indice ${i}, o tamanho do array é ${frutas.length}`)

console.log()

for (let fruta of frutas) {
    console.log(fruta)
    
}