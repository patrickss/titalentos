const rl = require('readline-sync')

const numero1 = Number(rl.question(`número1: `))
const numero2 = Number(rl.question(`número2: `))

console.log(
    `
    O primeiro numero é maior que segundo? ${numero1 > numero2}
    O primeiro numero é igual ao segundo? ${numero1 === numero2}
    O primeiro numero é divisível pelo segundo? ${(numero1 % numero2) == 0}
    O segundo numero é divisível pelo primeiro? ${(numero2%numero1) == 0} 
    `
)