const rl = require('readline-sync')

let numero = Number(rl.question('Digite um numero entre 0 e 10:'))
 if (numero >= 1 && numero <= 10) {
   for(i = 1; i <= numero; i++){
      console.log("*".repeat(i))
   } 
 } else {
   console.log("Desculpe, mas número não é válido, Tchau!!")
 }
 