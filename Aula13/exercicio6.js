const rl = require('readline-sync')
const segredo = Number(16)

console.log('Advinhe o número que pensei!!')

let numero
   
while (numero != segredo) {
    numero = Number(rl.question('Digite um numero entre 0 e 99: '))
    if (numero > segredo) {
        console.log("Tente um número menor!")
    } if (numero < segredo) {
        console.log("Tente um número maior!")
    } if (numero == segredo) {
        console.log("Parabéns voce advinhou!!!")
    }
}
// o ultimo "if" não precisa, posso colocar fora do "while" apenas o "console.log" final.