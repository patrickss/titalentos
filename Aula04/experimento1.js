console.log(
`Operações de adição:
${2 + 2}
${4 + 1.5}
${'ab' + 'cd'}
${'ab' + 2}

Operações de subtração:
${2 - 2}
${4 -  1.5}
${'ab' - 'cd'}
${'ab' - 2}
`
)


console.log()
console.log('ou como na outra ideia')
console.log()


console.log(
`Operação de Adição
2 + 2 = ${2 + 2}
4 + 1.5 = ${4 + 1.5}
'ab' + 'cd' = ${'ab' + 'cd'}
'ab' + 2 = ${'ab' + 2}

Operação de subtração
2 -  2 = ${2 - 2}
4 - 1.5 = ${4 - 1.5}
'ab' - 'cd' = ${'ab' - 'cd'}
'ab' - 2 = ${'ab' - 2}
`
)
