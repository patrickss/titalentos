init = function()
  
  blades = [200,200,400]
  passed = [0,0,0]
  
  gameover = 0
  score = 0
  position = 0
end

update = function()
  
if gameover>0 then
    gameover = gameover+1
    if gameover>300 then init() end
else  
  
position = position+2

player_y = player_y + player_vy

player_y = max(0,player_y + player_vy)

player_vy = player_vy - 0.19




if touch.touching and player_y ==0 then
  player_vy = 7 
  
  for i=0 to blades.length-1
    if blades[i]<position-120 then
      blades[i] = position+280+random.next()*200
      passed[i] = 0
    end
  end

  
  end
end

if abs(position-blades[i])<10 then
      if player_y<10 then
        gameover = 1
      elsif not passed[i] then
        passed[i] = 1
        score += 1
      end
    end
end


draw = function()

screen.fillRect (0,0,screen.width,screen.height,"rgb(255,255,255)")

screen.drawSprite("car",-80,-60 + player_y,90)

screen.drawSprite("sky",130 5-40,100)

screen.drawSprite("sky2",-100,60,70)

for i=0 to blades.length-1
    screen.drawSprite("ouch",blades[i]-position-80,-70,20)
end


for i=-6 to 6 by 1
  screen.drawSprite("platform",i*40-position%40,-80,40)
end

screen.drawText(score,120,80,20,"rgb(25,25,25)")
 if gameover then
    screen.fillRect(0,0,screen.width,screen.height,"rgba(255,0,0,.5)")
    screen.drawText("GAME OVER",0,0,50,"#FFF")
  end

end

 
