Interestellar = {
    titulo: 'Interestellar',
    diretor: 'Christopher Nolan',
    roteirista: 'jonathan Nolan, Christopher Nolan',
    elenco: [
        {
            nome: 'Matthew McCobaughey',
            papel: 'Cooper'

        },
        {
            nome: 'Anne Hathaway',
            papel: 'Brand'

        },
        {
            nome: 'Jessica Chastain',
            papel: 'Murph'
        }],
    ano: 2014,
    jahVi: true
}

console.log(
    `
    Filme: ${Interestellar.titulo}
    Ano: ${Interestellar.ano}
    Direção: ${Interestellar.diretor}
    Elenco: ${Interestellar.elenco[0].nome}
    ${Interestellar.jahVi ? 'Sim, já assisti' : 'Não, não assisti'}
    `
)