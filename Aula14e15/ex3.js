const donoDePet = {
    nome: 'Vanessa',
    pet: {
        nome:'Arya',
        raca: 'Colie',
        idade: 2
    }
}

console.log(
    `
    O nome do dono do Pet é ${donoDePet.nome}
    E ele é dono do Pet ${donoDePet.pet.nome}
    Seu Pet tem ${donoDePet.pet.idade} anos
    E é da Raça ${donoDePet.pet.raca}
    `    
)