Interestellar = {
    titulo: 'Interestellar',
    diretor: 'Christopher Nolan',
    roteirista: 'jonathan Nolan, Christopher Nolan',
    elenco: ['Matthew McCobaughey', 'Anne Hathaway', 'Jessica Chastain'],
    ano: 2014,
    jahVi: true

}

console.log(
    `
    Filme: ${Interestellar.titulo}
    Ano: ${Interestellar.ano}
    Direção: ${Interestellar.diretor}
    Elenco: ${Interestellar.elenco}
    ${Interestellar.jahVi ? 'Sim, já assisti' : 'Não, não assisti'}
    `
)