pessoa = {
    nome: 'Patrick Silva',
    idade: 27,
    generoMusical: 'Pop/Funk',
    apresentacao: function() {
        console.log('O nome da pessoa é '+this.nome+', ela tem '+this.idade+'anos e gosta muito de '+this.generoMusical)
    }
} 
pessoa.apresentacao()