let x = 10;
let y;

{
   let x = 20;
   console.log(`x dentro do bloco = ${x}`);
   y = 30;
   console.log(`y dentro do bloco = ${y}`);
}

console.log(`x fora do bloco = ${x}`);
console.log(`y fora do bloco = ${y}`);

