const rl = require('readline-sync')

const tarefas = [
    
]

function cadastrar() {
    const tarefa = rl.question('nova tarefa: ')
    tarefas.push(tarefa)
    console.log('Cadastrado')
}
function listar() {
    console.table(tarefas)
}
function modificar() {
    console.table(tarefas)
    const indice = rl.question('Qual tarefa? ')
    const tarefa = rl.question('Modificação')
    tarefas[indice] = tarefa
    console.log('tarefa modificada')
}
function excluir(params) {
    console.table(tarefas)
    const indice = rl.question('Indice da Tarefa concluida: ')
    const tarefaexcluida = tarefas.splice(indice,1)
    console.log(`Exclui a tarefa ${tarefaexcluida}`)
}

while (true) {
    
console.log(
    `
    CONTROLE DE TAREFAS
    ----------
    (L)istar Tarefas
    (C)adastrar Tarefa
    (M)odificar Tarefas
    (E)xcluir
    (S)air
    `        
    )
const comando = rl.question('?').toLocaleUpperCase()
if (comando == 'S') {
    break
} else if (comando == 'C') {
    cadastrar()
    
} else if (comando == 'L'){
    listar()

} else if (comando == 'M') {
    modificar()
    
} else if (comando == 'E') {
    excluir()
}
rl.question('Pressione ENTER para continuar')
}

console.log('Fim!')
