/* const rl = require('readline-sync');

let continuar = 's';

let i=0
while(i<10) {
        continuar = rl.question('Continuar (s/n)? ');
        i++
}

console.log('Fim do programa');
*/

/*
const rl = require('readline-sync');
const tarefas = [];

let continuar = 's';

while(continuar=='s') {
        // Pedimos a tarefa
        const tarefa = rl.question('Digite uma tarefa: ');
        // Adicionamos ela ao array de tarefas
        tarefas.push(tarefa);

        continuar = rl.question('Continuar (s/n)? ');
}

console.table(tarefas)
console.log('Fim do programa');
*/

 const rl = require('readline-sync');
const fs = require('fs');

const tarefas = [];

let continuar = 's';

while(continuar=='s') {
        // Pedimos a tarefa
        const tarefa = rl.question('Digite uma tarefa: ');
        // Adicionamos ela ao array de tarefas
        tarefas.push(tarefa);

        continuar = rl.question('Continuar (s/n)? ');
}

// Ao final do programa, iteramos sobre o array...

for (posicao=0; posicao<tarefas.length; posicao++) {
    const tarefa = tarefas[posicao];
    fs.appendFileSync('./tarefas.txt', `${tarefa}\n`);
}

console.log('Fim do programa');
console.table(tarefas)
